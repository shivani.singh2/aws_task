#!/bin/bash
rds_instance=("database-1" "database-2" "database-3")
for instance in ${rds_instance[@]};
do
aws rds stop-db-instance --db-instance-identifier $instance
done
