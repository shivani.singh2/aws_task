#!/bin/bash
instances=("i-07bb621a396499938" "i-06fe6fd303fe50154" "i-010deab61264d5b1c" "i-0b4c8d328d831d832" "i-0e4ab18af1259d473")

for instance in ${instances[@]};
do
aws ec2 stop-instances --instance-ids $instance
done
sleep 30

for instance in ${instances[@]};
do
aws ec2 modify-instance-attribute --instance-id $instance --instance-type "{\"Value\": \"c5.2xlarge\"}"
done
sleep 30

for instance in ${instances[@]};
do
aws ec2 start-instances --instance-ids $instance
done

